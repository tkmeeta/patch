package cn.chendd.patch.ui.adapters;

import java.awt.AWTException;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import cn.chendd.patch.ui.MainFrame;
import cn.chendd.utils.MyImageIcon;

public class TrayIconAdapter extends WindowAdapter {

	private MainFrame main;
	
	public TrayIconAdapter(MainFrame main) {
		this.main = main;
	}

	@Override
	public void windowIconified(WindowEvent evt) {
		if(SystemTray.isSupported() && main.trayIcon == null){
			SystemTray systemTray = SystemTray.getSystemTray();
			MyImageIcon icon = new MyImageIcon("tray.png");
			main.trayIcon = new TrayIcon(icon.getImage(), "补丁整理自动化", main.popup);
			try {
				systemTray.add(main.trayIcon);
			} catch (AWTException e) {
				e.printStackTrace();
			}
			//添加双击左键弹出菜单显示
			main.trayIcon.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if(e.getButton() == 1 && e.getClickCount() == 2){
						//点击的左键 && 点击了2次
						main.getFrame().setVisible(true);
						main.getFrame().setExtendedState(JFrame.NORMAL);
					}
				}
				
			});
			
		}
		main.getFrame().setVisible(false);
	}

	
}
