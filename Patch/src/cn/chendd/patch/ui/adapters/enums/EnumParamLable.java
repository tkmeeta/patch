package cn.chendd.patch.ui.adapters.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * <pre>
 * 每个table页参数名称管理
 * </pre>
 *
 * <pre>
 * modify by jiajt on 2017-6-28
 *    fix->1.
 *         2.
 * </pre>
 */
public enum EnumParamLable {

	BASIC_PARAM_SRC("src名称：","tool.param.basic.src","pathImpl"),
	BASIC_PARAM_SRC_IMPLEMENTS("src实现：","tool.param.basic.src.implement","pathImpl"),
	BASIC_PARAM_WEBCONTENT("WebRoot名称：","tool.param.basic.webRoot","pathImpl"),
	BASIC_PARAM_WEBCONTENT_IMPLEMENTS("WebRoot实现：","tool.param.basic.webRoot.implement","pathImpl"),
	BASIC_PARAM_WEBCONTENT_OTHER("其它实现：","tool.param.basic.other","pathImpl"),
	;
	
	
	private String  lableText;
	private String  lableTextProperCode;
	private String  paramType;
	
	EnumParamLable(String lableText,String lableTextProperCode,String paramType){
		this.lableText=lableText;
		this.lableTextProperCode=lableTextProperCode;
		this.paramType=paramType;
	}

	public String getLableText() {
		return lableText;
	}

	public String getLableTextProperCode() {
		return lableTextProperCode;
	}

	public String getParamType() {
		return paramType;
	}
	
	public static  List<EnumParamLable> getValuesList(String paramType){
		EnumParamLable[] enumParamLable=EnumParamLable.values();
		List<EnumParamLable> enumParamLableList = new ArrayList<EnumParamLable>();
		for (EnumParamLable ep : enumParamLable) {
			if(ep.getParamType().equals(paramType)){
				enumParamLableList.add(ep);
			}
		}
		return enumParamLableList;
	}
	
}
