package cn.chendd.patch.ui.adapters.enums;

import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cn.chendd.patch.enums.EnumProperties;
import cn.chendd.utils.MyImageIcon;

public class ConfigParamPanel extends SetPanel {

	private static final long serialVersionUID = -2116784751672842653L;

	public ConfigParamPanel(SetFrameAdapter setFrame) {
		super(setFrame);
	}

	@Override
	protected void initElement() {
		JPanel paramSmbPanel = this;
		paramSmbPanel.setLayout(null);
		paramSmbPanel.setBounds(10, 10, super.width, super.height);
		List<EnumParamLable> enumParamLable = EnumParamLable.getValuesList(EnumParamLable.BASIC_PARAM_WEBCONTENT.getParamType());
		for (int i = 0 , size = enumParamLable.size(); i < size; i++) {
			EnumParamLable e = enumParamLable.get(i);
			// lable名称
			JLabel nameLabel = new JLabel(e.getLableText());
			nameLabel.setFont(font);
			nameLabel.setBounds(10, 10 + i * 32, 100, 30);
			// 加文本框
			JTextField nameText = new JTextField();
			nameText.setBounds(105, 12 + i * 32, 220, 26);
			nameText.setText(getProperty(e.getLableTextProperCode()));
			nameText.setName(e.getLableTextProperCode());
			// 加入panel
			paramSmbPanel.add(nameLabel);
			paramSmbPanel.add(nameText);
		}
		
		JPanel buttonPanel = new JPanel();
		JButton saveButton = new JButton("保存");
		saveButton.setIcon(new MyImageIcon("save.gif"));
		saveButton.addMouseListener(new SaveParamPropsAdapter(this , EnumProperties.CONFIG_PROPERTIES));
		buttonPanel.add(saveButton);
		buttonPanel.setBounds(17, 50 + (enumParamLable.size()-1) * 30, super.height - 55, 50);
		paramSmbPanel.add(buttonPanel);
		
	}

}
