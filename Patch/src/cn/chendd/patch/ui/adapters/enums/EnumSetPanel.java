package cn.chendd.patch.ui.adapters.enums;

import java.util.Arrays;

/**
 * <pre>
 * 参数设置,按照默认的显示顺序
 * </pre>
 *
 * <pre>
 * modify by jiajt on 2017-6-26
 *    fix->1.
 *         2.
 * </pre>
 */
public enum EnumSetPanel {

	CONFIG_PARAM_PANEL("基本设置" , ConfigParamPanel.class,1),
	REPLACE_PARAM_PANEL("其它实现替换参数" , ReplaceParamPanel.class,2),
	;
	
	private String text;
	private Class<? extends SetPanel> clazz;
	private Integer order ;
	
	private EnumSetPanel(String text , Class<? extends SetPanel> clazz,Integer order){
		this.text = text;
		this.clazz = clazz;
		this.order=order;
	}

	public Integer getOrder() {
		return order;
	}


	public String getText() {
		return text;
	}

	public Class<?> getClazz() {
		return clazz;
	}
	
	public static  EnumSetPanel[] getValues(){
		EnumSetPanel[] enumSetPanel=EnumSetPanel.values();
		Integer[] orderInteArrays=new Integer[enumSetPanel.length];
		EnumSetPanel[] enumSetPanelNew=new EnumSetPanel[enumSetPanel.length];
		int i=0;
		for (EnumSetPanel es : enumSetPanel) {
			orderInteArrays[i]=es.getOrder();
			i++;
		}
		Arrays.sort(orderInteArrays);
		for (int j = 0; j < orderInteArrays.length; j++) {
			for (EnumSetPanel es : enumSetPanel) {
				if(orderInteArrays[j].equals(es.getOrder())){
					enumSetPanelNew[j]=es;
					break;
				}
			}
		}
		return enumSetPanelNew;
	}
	
	
	
	
	
}