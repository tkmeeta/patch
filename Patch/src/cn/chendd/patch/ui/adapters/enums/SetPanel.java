package cn.chendd.patch.ui.adapters.enums;

import java.awt.Font;
import java.util.Properties;

import javax.swing.JPanel;

import cn.chendd.patch.enums.EnumProperties;
import cn.chendd.utils.PropertiesUtil;

public abstract class SetPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	PropertiesUtil propertiesUtil= new PropertiesUtil(EnumProperties.CONFIG_PROPERTIES);
	/**
	 * 参数配置文件
	 */
	protected Properties configProperties = propertiesUtil.getProperties();

	protected String configFilePath =propertiesUtil.getPropertiesByPath();
	
	protected SetFrameAdapter setFrame;
	protected Font font = new Font("宋体" , Font.PLAIN , 13);
	
	public SetPanel(SetFrameAdapter setFrame) {
		this.setFrame = setFrame;
	}
	
	protected int width , height;
	
	public void init(){
		this.width = setFrame.setDialog.getWidth();
		this.height = setFrame.setDialog.getHeight();
		initElement();
	}
	
	protected abstract void initElement();
	
	/**
	 * <pre>
	 * 根据key读取相应的参数设置
	 * </pre>
	 */
	public String getProperty(String key){
		return this.getProperty(key , null);
	}
	
	/**
	 * <pre>
	 * 根据key读取对应的参数设置,为空则赋默认值
	 * </pre>
	 */
	public String getProperty(String key , String defaultValue){
		return configProperties.getProperty(key , defaultValue);
	}
	
}
