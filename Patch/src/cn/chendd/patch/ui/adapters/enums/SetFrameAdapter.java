package cn.chendd.patch.ui.adapters.enums;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JTabbedPane;

import org.apache.commons.beanutils.ConstructorUtils;

import cn.chendd.base.tools.Property;
import cn.chendd.patch.ui.MainFrame;
import cn.chendd.utils.MyImageIcon;

/**
 * 
 * <pre>
 * 
 * </pre>
 *
 * <pre>
 * modify by jiajt on 2017-6-26
 *    fix->1.
 *         2.
 * </pre>
 */
public class SetFrameAdapter implements ActionListener {

	public JDialog setDialog;
	public JButton loadButton;
	
	private MainFrame mainFrame;
	
	public SetFrameAdapter(MainFrame mainFrame){
		this.mainFrame = mainFrame;
	}

	private void initTabs() {
		
		JTabbedPane tab = new JTabbedPane();
		tab.setBounds(0, 0,setDialog.getWidth() - 8, setDialog.getHeight() - 28);
		EnumSetPanel panels[] = EnumSetPanel.getValues();
		for (EnumSetPanel enumPanel : panels) {
			Class<?> clazz = enumPanel.getClazz();
			try {
				SetPanel panel = (SetPanel) ConstructorUtils.invokeConstructor(clazz, SetFrameAdapter.this);
				Field fields[] = clazz.getDeclaredFields();
				for (Field field : fields) {
					Property property = field.getAnnotation(Property.class);
					if(property == null){
						continue;
					}
					String key = property.value();
					String value = panel.getProperty(key);
					field.setAccessible(true);
					field.set(panel, value);
				}
				panel.init();
				tab.add(enumPanel.getText() , panel);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		setDialog.add(tab);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

		//弹出窗口
		setDialog = new JDialog(mainFrame.getMainFrame());
		setDialog.setLayout(null);
		setDialog.setModal(true);
		setDialog.setSize(mainFrame.windowWidth - 60, mainFrame.windowHeight - 220);
		setDialog.setLocationRelativeTo(mainFrame.getMainFrame());
		setDialog.setTitle("参数设置");
		setDialog.setIconImage(new MyImageIcon("logo.png").getImage());
		//初始化tab
		initTabs();
		setDialog.setVisible(true);
	}
	
	

}
