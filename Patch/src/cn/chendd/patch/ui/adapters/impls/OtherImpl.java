package cn.chendd.patch.ui.adapters.impls;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import cn.chendd.utils.LogUtil;

public class OtherImpl extends PathAdapter {

	public OtherImpl(String compileFolder, String regex, File dstFolder , String path) {
		super(compileFolder, regex, dstFolder, path);
	}

	@Override
	public void patchByPath() throws IOException {
		LogUtil.info(getClass().getSimpleName() + "实现匹配路径：" + path);
		File srcFile = new File(compileFolder , path);
		File newFile = new File(dstFolder , path);
		if(srcFile.isDirectory()){
			newFile.mkdirs();
		} else {
			if(srcFile.exists()){
				FileUtils.copyFile(srcFile, newFile);
			}
		}
	}

	/**
	 * 所有不匹配的项且没有被忽略的文件路径均采用此种默认的实现
	 */
	@Override
	public boolean patchFilterRule() {
		return true;
	}
	
}
