package cn.chendd.patch.ui.adapters.impls;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import cn.chendd.utils.LogUtil;

public class WebRootImpl extends PathAdapter {

	public WebRootImpl(String compileFolder , String regex , File dstFolder , String path) {
		super(compileFolder , regex , dstFolder , path);
	}

	@Override
	public void patchByPath() throws IOException {
		LogUtil.info(getClass().getSimpleName() + "实现匹配路径：" + path);
		String newPath = path.substring(regex.length());
		File srcFile = new File(compileFolder , newPath);
		File newFile = new File(dstFolder , newPath);
		if(srcFile.isDirectory()){
			newFile.mkdirs();
		} else {
			if(srcFile.exists()){
				FileUtils.copyFile(srcFile, newFile);
			}
		}
	}

	@Override
	public boolean patchFilterRule() {
		return path.startsWith(regex);
	}
	
}
