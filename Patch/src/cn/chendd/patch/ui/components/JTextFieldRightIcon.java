package cn.chendd.patch.ui.components;


import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JTextField;

import cn.chendd.utils.MyImageIcon;

/**
 * 带有右侧小图标的公共类
 * @author chendd
 *
 */
public class JTextFieldRightIcon extends JTextField {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String imagePath;
	
	public JTextFieldRightIcon(String imagePath){
		this.imagePath = imagePath;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int width = this.getWidth();
		int height = this.getHeight();
		Image image = new MyImageIcon(imagePath).getImage();
		int imageWidth = image.getWidth(null);
		int imageHeight = image.getHeight(null);
		int _height = (height - imageHeight) / 2;
		if(_height < 0){
			_height = 0;
		}
		g.drawImage(image, width - imageWidth - 5, _height, null);
	}
	
}
