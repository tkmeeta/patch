/**
 * 
 */
package cn.chendd.patch.ui.components.table;

import java.awt.Dimension;
import java.util.LinkedHashMap;
import java.util.List;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import sun.swing.table.DefaultTableCellHeaderRenderer;
import cn.chendd.patch.vo.DataVo;
import cn.chendd.utils.CommonUtil;
import cn.chendd.utils.LogUtil;

/** 
 * <pre>
 * 数据表格对象封装
 * </pre>
 *
 * <pre>
 * modify by chendd on 2017-6-20
 *    fix->1.
 *         2.
 * </pre> 
 */
public abstract class DataTable extends JTable {

	public static final long serialVersionUID = 1L;

	private DefaultTableModel model = new DefaultTableModel();
	
	/**
	 * 初始化表格，宽带、高度、是否自动填充数据
	 * @param windowWidth
	 * @param windowHeight
	 * @param fillData
	 */
	public DataTable(){
		
		this.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		this.setShowVerticalLines(true);//显示垂直线
		/*this.setEnabled(false);*/
		//默认先调用HashMap类型的取数函数，如获取不到再调用DataVo函数，如果都获取不到，则给给默认构造
		Object datas[][] = null;
		List<LinkedHashMap<String , Object>> dataMapList = this.getDataMapList();
		if(dataMapList == null || dataMapList.isEmpty()){
			datas = CommonUtil.listMap2Array(dataMapList);
		}
		if(datas == null){
			List<? extends DataVo> dataVoList = this.getDataVoList();
			datas = CommonUtil.list2Array(dataVoList);
		}
		if(datas == null){
			datas = new Object[][]{};
		}
		
		model.setDataVector(datas , this.getHeaders());
		resetTableStyle();
		this.setModel(model);
		//applyDataTable.setRowSorter(new TableRowSorter(model));
		Dimension ds = this.getIntercellSpacing();
		ds.setSize(ds.getWidth() + 15, ds.getHeight());
		this.setIntercellSpacing(ds);
		this.setRowHeight(25);
	}
	
	/**
	 * <pre>
	 * 设置表格数据
	 * </pre>
	 */
	public void fillDataMapList(){
		this.fillDataMapList(this.getDataMapList());
	}
	
	public void fillDataMapList(List<LinkedHashMap<String , Object>> dataList){
		Object datas[][] = CommonUtil.listMap2Array(dataList);
		model.setDataVector(datas, this.getHeaders());
		this.setModel(model);
		resetTableStyle();
	}

	protected void resetTableStyle() {
		DefaultTableCellHeaderRenderer headerRender = (DefaultTableCellHeaderRenderer) this.getTableHeader().getDefaultRenderer();
		headerRender.setHorizontalAlignment(SwingConstants.CENTER);
		//设置编号列居中
		int columnCount = this.getColumnModel().getColumnCount();
		if(columnCount != 0){
			TableColumn numColumn = this.getColumnModel().getColumn(0);
			numColumn.setMinWidth(50);
			numColumn.setMaxWidth(50);
			DefaultTableCellRenderer centerRender = new DefaultTableCellRenderer();
			centerRender.setHorizontalAlignment(SwingConstants.CENTER);
			numColumn.setCellRenderer(centerRender);
		}
		//设置列宽
		Integer columnWidths[] = this.getColumnWidth();
		if(columnWidths != null && columnCount != 0){
			//设置其他列宽，标题列除外
			for(int i=0 , lens = columnWidths.length ; i < lens ; i++){
				this.getColumnModel().getColumn(i).setMinWidth(columnWidths[i]);
			}
		}
		
		tableRender();
		
	}
	
	protected void tableRender() {}

	/**
	 * <pre>
	 * 设置表格数据
	 * </pre>
	 */
	public void fillDataVoList(){
		this.fillDataVoList(this.getDataVoList());
	}
	
	public void fillDataVoList(List<? extends DataVo> dataList){
		Object datas[][];
		try {
			datas = CommonUtil.list2Array(dataList);
		} catch (Exception e) {
			LogUtil.error("转换List<Bean>数据出现错误：", e);
			datas = new Object[][]{};
		}
		model.setDataVector(datas, this.getHeaders());
		this.setModel(model);
		resetTableStyle();
	}
	
	/**
	 * <pre>
	 * 自动根据取值函数填充数据
	 * </pre>
	 */
	public void autoFillData(){
		model.setDataVector(this.getAutoFillDatas(), this.getHeaders());
		this.setModel(model);
		resetTableStyle();
	}
	
	/**
	 * <pre>
	 * 自动获取数据
	 * </pre>
	 * @return
	 */
	private Object[][] getAutoFillDatas(){
		Object datas[][] = CommonUtil.listMap2Array(this.getDataMapList());
		if(datas == null){
			try {
				datas = CommonUtil.list2Array(this.getDataVoList());
			} catch (Exception e) {
				LogUtil.error("转换List<Bean>数据出现错误：", e);
			}
		}
		if(datas == null){
			datas = new Object[][]{};
		}
		return datas;
	}
	
	/**
	 * <pre>
	 * 设置列标题名
	 * </pre>
	 * @return
	 */
	public abstract String[] getHeaders();
	
	/**
	 * <pre>
	 * 标题列除外的其他列
	 * </pre>
	 * @return
	 */
	public abstract Integer[] getColumnWidth();
	
	/**
	 * <pre>
	 * 获取取数
	 * </pre>
	 * @return
	 */
	public abstract List<? extends DataVo> getDataVoList();
	
	/**
	 * <pre>
	 * 获取取数
	 * </pre>
	 * @return
	 */
	public abstract List<LinkedHashMap<String , Object>> getDataMapList();
	
}
