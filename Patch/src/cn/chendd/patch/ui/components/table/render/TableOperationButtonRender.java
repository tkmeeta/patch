package cn.chendd.patch.ui.components.table.render;


import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * <pre>
 * 表格的操作列渲染
 * </pre>
 *
 * <pre>
 * modify by chendd on 2017-6-30
 *    fix->1.
 *         2.
 * </pre>
 */
public class TableOperationButtonRender implements TableCellRenderer {

	private JPanel panel = new JPanel();
	private JButton resetImportButton;//重新导入
	private JButton placeImportButton;//置导入
	
	public TableOperationButtonRender() {
		
		panel.setLayout(null);
		resetImportButton = new JButton("重新导入");
		resetImportButton.setBounds(0, 2, 85, 20);
		placeImportButton = new JButton("置导入");
		placeImportButton.setBounds(90, 2, 70, 20);
		panel.add(resetImportButton);
		panel.add(placeImportButton);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		
		panel.setBackground(table.getBackground());
		return panel;
	}


}
