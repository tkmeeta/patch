package cn.chendd.patch.vo;

public abstract class DataVo {

	/**
	 * <pre>
	 * 对象属性的先后排序
	 * </pre>
	 * @return
	 */
	public abstract String[] getPropSorts();
	
}
