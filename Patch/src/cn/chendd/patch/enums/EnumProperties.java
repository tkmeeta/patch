package cn.chendd.patch.enums;

/**
 * 
 * <pre>
 * 配置文件定义
 * </pre>
 *
 * <pre>
 * modify by chendd on 2017-6-27
 *    fix->1.
 *         2.
 * </pre>
 */
public enum EnumProperties {

	DATABASE_PROPERTIES("database.properties"),
	CONFIG_PROPERTIES("config.properties"),
	REPLACE_PROPERTIES("replace.properties"),
	MYBATIS_CONFIG("configuration.xml"),
	;
	
	public final static String database_properties = EnumProperties.CONFIG_PROPERTIES.getFileName();
	
	private String fileName;
	
	private EnumProperties(String fileName){
		this.fileName = fileName;
	}

	/**
	 * @return Returns the fileName.
	 */
	public String getFileName() {
		return fileName;
	}
	
}
