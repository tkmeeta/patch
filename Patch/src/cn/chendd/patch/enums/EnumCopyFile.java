package cn.chendd.patch.enums;

/**
 * <pre>
 * 实现拷贝补丁文件
 * </pre>
 *
 * <pre>
 * modify by chendd on 2018-8-6
 *    fix->1.
 *         2.
 * </pre>
 */
public enum EnumCopyFile {

	SUFFIX_JAVA("java"),//Java文件后缀
	SUFFIX_OTHER("other"),//含有后缀的文件
	FOLDER("folder"),//文件夹
	;
	
	private EnumCopyFile(String type){
		
	}
	
}
