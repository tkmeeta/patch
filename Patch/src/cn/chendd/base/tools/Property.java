/**
 * 
 */
package cn.chendd.base.tools;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
 * <pre>
 * 参数注解
 * </pre>
 *
 * <pre>
 * modify by chendd on 2017-6-28
 *    fix->1.
 *         2.
 * </pre> 
 */
@Target(value = ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Property {

	public String value() default "";
	
}
