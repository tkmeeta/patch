package cn.chendd.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import cn.chendd.patch.enums.EnumProperties;

public class PropertiesUtil {

	private String fileName;
	
	public PropertiesUtil(String fileName){
		this.fileName = fileName;
	}
	
	public PropertiesUtil(EnumProperties enumProperties){
		this(enumProperties.getFileName());
	}
	
	public String getPropertiesByPath() {
		String userDir = System.getProperty("user.dir") + File.separator + "config"
				+ File.separator + fileName;
		return userDir;
	}
	
	public boolean hasConfigProperties(){
		String path = getPropertiesByPath();
		File file = new File(path);
		return file.exists();
	}
	
	public Properties getProperties(){
		boolean exist = hasConfigProperties();
		Properties props = new Properties();
		if(! exist){
			return props;
		}
		InputStream in = null;
		try {
			String path = getPropertiesByPath();
			File file = new File(path);
			in = new FileInputStream(file);
			props.load(in);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			CloseUtil.close(in);
		}
		return props;
	}
	
}
